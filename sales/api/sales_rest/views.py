from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
    SalesPersonEncoder,
    PotentialCustomerEncoder,
    SaleEncoder,
)
from .models import SalesPerson, PotentialCustomer, Sale, AutomobileVO

# Create your views here.


@require_http_methods(["GET", "POST"])
def list_sales_people(request):
    if request.method == "GET":
        sales_people = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_people": sales_people},
            encoder=SalesPersonEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            sales_person = SalesPerson.objects.create(**content)
        except Exception as exception:
            return JsonResponse(
                {"message": exception},
                status=400,
            )
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def show_sales_person(request, id):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(id=id)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Sales person not found"},
                status=404,
            )

    elif request.method == "DELETE":
        count, _ = SalesPerson.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            SalesPerson.objects.filter(id=id).update(**content)
            sales_person = SalesPerson.objects.filter(id=id)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except Exception as exception:
            return JsonResponse(
                {"message": exception},
                status=400,
            )


@require_http_methods(["GET", "POST"])
def list_potential_cutomers(request):
    if request.method == "GET":
        potential_customers = PotentialCustomer.objects.all()
        return JsonResponse(
            {"potential_customers": potential_customers},
            encoder=PotentialCustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            potential_customer = PotentialCustomer.objects.create(**content)
        except Exception as exception:
            return JsonResponse(
                {"message": exception},
                status=400,
            )

        return JsonResponse(
            potential_customer,
            encoder=PotentialCustomerEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def show_potential_cutomer(request, id):
    try:
        potential_customer = PotentialCustomer.objects.get(id=id)
    except PotentialCustomer.DoesNotExist:
        return JsonResponse({"error": "Potential customer not found"}, status=404)

    if request.method == "GET":
        return JsonResponse(
            potential_customer,
            encoder=PotentialCustomerEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = PotentialCustomer.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            PotentialCustomer.objects.filter(id=id).update(**content)
            potential_customer = PotentialCustomer.objects.filter(id=id)
            return JsonResponse(
                potential_customer,
                encoder=PotentialCustomerEncoder,
                safe=False,
            )
        except PotentialCustomer.DoesNotExist:
            return JsonResponse(
                {"error": "Potential customer not found"},
                status=404,
            )


@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            content["automobile"] = AutomobileVO.objects.get(
                import_href=content.get("automobile")
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile href"},
                status=400,
            )
        try:
            content["sales_person"] = SalesPerson.objects.get(
                employee_number=content.get("sales_person")
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid sales person number"},
                status=400,
            )
        try:
            content["customer"] = PotentialCustomer.objects.get(
                id=content.get("customer")
            )
        except PotentialCustomer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status=400,
            )
        try:
            sale = Sale.objects.create(**content)
        except Exception as exception:
            return JsonResponse(
                {"message": exception},
                status=400,
            )
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def show_sale(request, id):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=id)
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sale not found"},
                status=404,
            )
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            count, _ = Sale.objects.filter(id=id).delete()
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sale not found"},
                status=404,
            )
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            Sale.objects.filter(id=id).update(**content)
            sale = Sale.objects.filter(id=id)
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sale not found"},
                status=404,
            )
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )
