from django.urls import path
from .views import (
    api_list_appointments,
    api_list_technicians,
    api_show_appointment,
    api_show_technician,
    api_service_history,
)

urlpatterns = [
    path("appointment/", api_list_appointments, name="api_list_appointments"),
    path("technician/", api_list_technicians, name="api_list_technicians"),
    path("appointment/<int:pk>/", api_show_appointment, name="api_show_appointment"),
    path("technician/<int:pk>/", api_show_technician, name="api_show_technician"),
    path("vin/appointment/<str:vin>/", api_service_history, name="api_service_history"),
]
