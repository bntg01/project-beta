import React, { useState } from "react";

const TechnicianForm = () => {
  const [name, setName] = useState("");
  const [id, setId] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = { name, id };

    const technicianUrl = "http://localhost:8080/api/technician/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
      setName("");
      setId("");
      const success = document.getElementById("success-message");
      success.classList.remove("d-none");
    }
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    if (name === "name") {
      setName(value);
    } else {
      setId(value);
    }
  };

  return (
    <div className="container pt-5">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add New Technician</h1>
          <form onSubmit={handleSubmit} id="create-technician-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleInputChange}
                value={name}
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleInputChange}
                value={id}
                placeholder="Technician ID<"
                required
                type="number"
                name="id"
                id="id"
                className="form-control"
              />
              <label htmlFor="id">Technician ID</label>
            </div>

            <button className="btn btn-success">Add</button>
          </form>
          <div className="alert alert-success d-none mt-5" id="success-message">
            Added a new technician!
          </div>
        </div>
      </div>
    </div>
  );
};

export default TechnicianForm;
