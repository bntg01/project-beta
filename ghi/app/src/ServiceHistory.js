import React, { useState, useEffect } from "react";

const ServiceHistory = () => {
  const [appointments, setAppointments] = useState([]);
  const [search, setSearch] = useState("");
  const [Results, setResults] = useState([]);

  const Appointments = async () => {
    let response = await fetch("http://localhost:8080/api/appointment/");
    let data = await response.json();
    setAppointments(data.appointments);
    setResults(data.appointments);
  };

  useEffect(() => {
    Appointments();
  }, []);

  useEffect(() => {
    setResults(
      appointments.filter((appointment) =>
        appointment.vin.includes(search.toUpperCase())
      )
    );
  }, [search, appointments]);

  return (
    <>
      <form className="mx-2 my-auto d-inline w-100">
        <input
          type="text"
          className="form-control border border-right-0"
          placeholder="Enter Vin Number"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
      </form>
      <h1 style={{ fontSize: "30px", color: "gray" }}>Service History</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Vin</th>
            <th>Customer Name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Tech</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
          {Results.map((appointment) => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.owner}</td>
                <td>{appointment.date}</td>
                <td>{appointment.time}</td>
                <td>{appointment.technician.name}</td>
                <td>{appointment.reason}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
};

export default ServiceHistory;
