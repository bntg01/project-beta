import React, { useState, useEffect } from "react";

const PotentialCustomersList = () => {
  const [potentialCustomers, setPotentialCustomers] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(
        "http://localhost:8090/sales/potential_cutomers/"
      );
      if (response.ok) {
        const data = await response.json();
        setPotentialCustomers(data.potential_customers);
      } else {
        console.error(response);
      }
    };

    fetchData();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Address</th>
          <th>Phone Number</th>
        </tr>
      </thead>
      <tbody>
        {potentialCustomers.map((potentialCustomer) => {
          return (
            <tr key={potentialCustomer.phone_number}>
              <td>{potentialCustomer.name}</td>
              <td>{potentialCustomer.address}</td>
              <td>{potentialCustomer.phone_number}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default PotentialCustomersList;
