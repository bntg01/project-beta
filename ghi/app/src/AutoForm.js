import React, { useEffect, useState } from "react";

const AutoForm = () => {
  const [state, setState] = useState({
    color: "",
    year: "",
    vin: "",
    model_id: "",
    models: [],
  });

  const handleChange = (event) => {
    const newState = {
      ...state,
      [event.target.id]: event.target.value,
    };
    setState(newState);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = { ...state };
    delete data.models;

    const AUTO_URL = "http://localhost:8100/api/automobiles/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(AUTO_URL, fetchConfig);
    if (response.ok) {
      setState((prevState) => ({
        ...prevState,
        color: "",
        year: "",
        vin: "",
        model_id: "",
      }));
    } else {
      console.error(response);
    }
  };

  useEffect(() => {
    const fetchModels = async () => {
      const URL = "http://localhost:8100/api/models/";

      const response = await fetch(URL);

      if (response.ok) {
        const data = await response.json();
        setState({ models: data.models });
      } else {
        console.error(response);
      }
    };
    fetchModels();
  }, []);

  const models = state.models.map((model) => {
    return (
      <option key={model.id} value={model.id}>
        {model.name}
      </option>
    );
  });

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add an automobile to inventory</h1>
          <form onSubmit={handleSubmit} id="create-auto-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleChange}
                value={state.color || ""}
                placeholder="Color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleChange}
                value={state.year || ""}
                placeholder="Year"
                required
                type="text"
                name="year"
                id="year"
                className="form-control"
              />
              <label htmlFor="year">Year</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleChange}
                value={state.vin || ""}
                placeholder="VIN"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="mb-3">
              <select
                onChange={handleChange}
                value={state.model_id || ""}
                required
                name="model_id"
                id="model_id"
                className="form-select"
              >
                <option value="">Choose a model</option>
                {models}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default AutoForm;
