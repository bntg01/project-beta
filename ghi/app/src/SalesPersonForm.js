import React, { useState } from "react";

const SalesPersonForm = () => {
  const [formData, setFormData] = useState({
    name: "",
    employee_number: "",
  });

  const handleChange = (event) => {
    const newState = {};
    newState[event.target.id] = event.target.value;
    setFormData({ ...formData, ...newState });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = { ...formData };

    const SALES_PEOPLE_URL = "http://localhost:8090/sales/sales_people/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(SALES_PEOPLE_URL, fetchConfig);
    if (response.ok) {
      setFormData({
        name: "",
        employee_number: "",
      });
    } else {
      console.error(response);
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new sales person</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleChange}
                value={formData.name}
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleChange}
                value={formData.employee_number}
                placeholder="Employee number"
                required
                type="number"
                min="0"
                name="employee_number"
                id="employee_number"
                className="form-control"
              />
              <label htmlFor="employee_number">Employee number</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default SalesPersonForm;
