export const menuItems = [
  {
    title: "Inventory",
    submenu: [
      {
        title: "Automobiles",
        submenu: [
          {
            title: "View Automobiles",
            url: "/autos",
          },
          {
            title: "Create New Automobile",
            url: "/autos/new",
          },
        ],
      },
      {
        title: "Models",
        submenu: [
          {
            title: "View Models",
            url: "/models",
          },
          {
            title: "New Model",
            url: "/models/new",
          },
        ],
      },
      {
        title: "Manufacturers",
        submenu: [
          {
            title: "View Manufacturers",
            url: "/manufacturers",
          },
          {
            title: "New Manufacturer",
            url: "/manufacturers/new",
          },
        ],
      },
    ],
  },
  {
    title: "Sales",
    submenu: [
      {
        title: "Sales People",
        submenu: [
          {
            title: "View Sales People",
            url: "/sales_people",
          },
          {
            title: "New Sales Person",
            url: "/sales_people/new",
          },
        ],
      },
      {
        title: "Potential Customers",
        submenu: [
          {
            title: "View Potential Customers",
            url: "/potential_customers",
          },
          {
            title: "New Potential Customer",
            url: "/potential_customers/new",
          },
        ],
      },
      {
        title: "Sales",
        submenu: [
          {
            title: "View Sales",
            url: "/sales",
          },
          {
            title: "New Sale",
            url: "/sales/new",
          },
          {
            title: "Sales History",
            url: "/history",
          },
        ],
      },
    ],
  },
  {
    title: "Service",
    submenu: [
      {
        title: "Appointments",
        submenu: [
          {
            title: "View Appointments",
            url: "/appointment/",
          },
          {
            title: "New Appointment",
            url: "/appointment/new",
          },
          {
            title: "Appointment History",
            url: "/appointment/history",
          },
        ],
      },
      {
        title: "Techs",
        submenu: [
          {
            title: "New Tech",
            url: "/technician/new",
          },
        ],
      },
    ],
  },
];
