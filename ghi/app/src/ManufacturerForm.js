import React, { useState } from "react";

const ManufacturerForm = () => {
  const [formState, setFormState] = useState({
    name: "",
  });

  const handleChange = (event) => {
    const value = event.target.value;
    setFormState({ ...formState, [event.target.name]: value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = { ...formState };
    const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(manufacturerUrl, fetchConfig);
    if (response.ok) {
      setFormState({
        name: "",
      });
    } else {
      console.error(response);
    }
  };

  return (
    <div>
      <h1>Create a new car manufacturer</h1>
      <form onSubmit={handleSubmit} id="">
        <div className="form-floating mb-3">
          <input
            onChange={handleChange}
            placeholder="Name"
            required
            type="text"
            name="name"
            id="name"
            className="form-control"
            value={formState.name}
          />
          <label htmlFor="name">Name</label>
        </div>
        <button className="btn btn-primary">Create</button>
      </form>
    </div>
  );
};

export default ManufacturerForm;
