import React, { useState, useEffect } from "react";

const SalesPersonHistory = () => {
  const [salesPeople, setSalesPeople] = useState([]);
  const [salesPerson, setSalesPerson] = useState("");
  const [sales, setSales] = useState([]);

  const handleChange = (event) => {
    setSalesPerson(event.target.value);
  };

  useEffect(() => {
    const fetchSalesPeople = async () => {
      const SALES_PEOPLE_URL = "http://localhost:8090/sales/sales_people/";
      const salesPeopleResponse = await fetch(SALES_PEOPLE_URL);
      if (salesPeopleResponse.ok) {
        const data = await salesPeopleResponse.json();
        setSalesPeople(data.sales_people);
      } else {
        console.error(salesPeopleResponse);
      }
    };
    fetchSalesPeople();
  }, []);

  useEffect(() => {
    const fetchSales = async () => {
      const SALES_URL = "http://localhost:8090/sales/sales/";
      const salesResponse = await fetch(SALES_URL);
      if (salesResponse.ok) {
        const data = await salesResponse.json();
        setSales(data.sales);
      } else {
        console.error(salesResponse);
      }
    };
    fetchSales();
  }, []);

  useEffect(() => {
    const filterSalesBySalesPerson = async () => {
      const SALES_URL = "http://localhost:8090/sales/sales/";
      const response = await fetch(SALES_URL);
      if (response.ok) {
        const data = await response.json();
        setSales(
          data.sales.filter((sale) => sale.sales_person.name === salesPerson)
        );
      } else {
        console.error(response);
      }
    };
    if (salesPerson) {
      filterSalesBySalesPerson();
    }
  }, [salesPerson]);

  return (
    <div>
      <h1>Sales person history</h1>
      <div className="mb-3">
        <select
          onChange={handleChange}
          value={salesPerson}
          required
          name="sales_person"
          id="sales_person"
          className="form-select"
        >
          <option value="">Choose a sales person</option>
          {salesPeople.map((sales_person) => {
            return (
              <option
                key={sales_person.employee_number}
                value={sales_person.name}
              >
                {sales_person.name}
              </option>
            );
          })}
        </select>
      </div>
      {salesPerson && (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Sales Person</th>
              <th>Employee Number</th>
              <th>Purchaser's Name</th>
              <th>Automobile VIN</th>
              <th>Price of Sale</th>
            </tr>
          </thead>
          <tbody>
            {sales.map((sale) => {
              return (
                <tr key={sale.automobile.vin}>
                  <td>{sale.sales_person.name}</td>
                  <td>{sale.sales_person.employee_number}</td>
                  <td>{sale.customer.name}</td>
                  <td>{sale.automobile.vin}</td>
                  <td>
                    {sale.sale_price.toLocaleString("en-US", {
                      style: "currency",
                      currency: "USD",
                    })}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      )}
    </div>
  );
};

export default SalesPersonHistory;
