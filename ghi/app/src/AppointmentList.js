import { Link } from "react-router-dom";
import React, { useState, useEffect } from "react";

const AppointmentList = () => {
  const [appointments, setAppointments] = useState([]);

  useEffect(() => {
    const fetchAppointments = async () => {
      const response = await fetch("http://localhost:8080/api/appointment/");
      if (response.ok) {
        const data = await response.json();
        const filterAppointments = data.appointments.filter(
          (appointment) => appointment.finished === false
        );
        setAppointments(filterAppointments);
      } else {
        console.error(response);
      }
    };
    fetchAppointments();
  }, []);

  const cancelAppointment = async (id) => {
    const appointmentUrl = `http://localhost:8080/api/appointment/${id}/`;

    const fetchConfig = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
      const fetchAppointments = async () => {
        const response = await fetch("http://localhost:8080/api/appointment/");
        if (response.ok) {
          const data = await response.json();
          const filterAppointments = data.appointments.filter(
            (appointment) => appointment.finished === false
          );
          setAppointments(filterAppointments);
        } else {
          console.error(response);
        }
      };
      fetchAppointments();
    }
  };

  const finishedAppointment = async (id) => {
    const appointmentUrl = `http://localhost:8080/api/appointment/${id}/`;

    const fetchConfig = {
      method: "PUT",
      body: JSON.stringify({ finished: true }),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
      const fetchAppointments = async () => {
        const response = await fetch("http://localhost:8080/api/appointment/");
        if (response.ok) {
          const data = await response.json();
          const filterAppointments = data.appointments.filter(
            (appointment) => appointment.finished === false
          );
          setAppointments(filterAppointments);
        } else {
          console.error(response);
        }
      };
      fetchAppointments();
    }
  };

  let appointmentHistory;
  if (appointments.length === 0) {
    appointmentHistory = <p>No appointments yet</p>;
  } else {
    appointmentHistory = (
      <table>
        <thead>
          <tr>
            <th>Vin</th>
            <th>Customer Names</th>
            <th>Date</th>
            <th>Time</th>
            <th>Reason</th>
            <th>Tech</th>
            <th>Vip</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointment) => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.owner}</td>
                <td>{appointment.date}</td>
                <td>{appointment.time}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.technician.name}</td>
                {appointment.vip ? <td>Yes</td> : <td>No</td>}
                <td>
                  <button
                    className="btn btn-outline-danger"
                    onClick={() => cancelAppointment(appointment.id)}
                  >
                    Cancel Appointment
                  </button>
                </td>
                <td>
                  <button
                    className="btn btn-outline-success"
                    onClick={() => finishedAppointment(appointment.id)}
                  >
                    Finished
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
  return (
    <div className="container pt-5">
      <h1>Service Appointments</h1>
      {appointmentHistory}
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-end">
        <Link
          to="/appointment/new"
          className="btn btn-success btn-lg px-4 gap-3"
        >
          Schedule an appointment!
        </Link>
      </div>
    </div>
  );
};

export default AppointmentList;
