import React, { useState, useEffect } from "react";

const SaleList = () => {
  const [sales, setSales] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const response = await fetch("http://localhost:8090/sales/sales/");
      if (response.ok) {
        const data = await response.json();
        setSales(data.sales);
      } else {
        console.error(response);
      }
    }

    fetchData();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Sales Person</th>
          <th>Employee Number</th>
          <th>Purchaser's Name</th>
          <th>Automobile VIN</th>
          <th>Price of Sale</th>
        </tr>
      </thead>
      <tbody>
        {sales.map((sale) => {
          return (
            <tr key={sale.automobile.vin}>
              <td>{sale.sales_person.name}</td>
              <td>{sale.sales_person.employee_number}</td>
              <td>{sale.customer.name}</td>
              <td>{sale.automobile.vin}</td>
              <td>
                {sale.sale_price.toLocaleString("en-US", {
                  style: "currency",
                  currency: "USD",
                })}
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default SaleList;
