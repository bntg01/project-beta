import { NavLink } from "react-router-dom";
import { menuItems } from "./MenuItems";
import { Dropdown } from "react-bootstrap";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            {menuItems.map((menu, index) => {
              return (
                <Dropdown key={index}>
                  <Dropdown.Toggle variant="success" id="dropdown-basic">
                    {menu.title}
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    {menu.submenu?.map((submenu, index) => {
                      return (
                        <Dropdown key={index} drop="end">
                          <Dropdown.Toggle
                            variant="success"
                            id="dropdown-basic"
                          >
                            {submenu.title}
                          </Dropdown.Toggle>
                          <Dropdown.Menu>
                            {submenu.submenu?.map((subsubmenu, index) => {
                              return (
                                <Dropdown.Item
                                  as={NavLink}
                                  to={subsubmenu.url}
                                  style={{
                                    color: "black",
                                    backgroundColor: "white",
                                  }}
                                  className="nav-link"
                                  key={index}
                                >
                                  {subsubmenu.title}
                                </Dropdown.Item>
                              );
                            })}
                          </Dropdown.Menu>
                        </Dropdown>
                      );
                    })}
                  </Dropdown.Menu>
                </Dropdown>
              );
            })}
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
