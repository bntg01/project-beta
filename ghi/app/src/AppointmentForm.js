import React, { useState, useEffect } from "react";

const AppointmentForm = () => {
  const [formState, setFormState] = useState({
    vin: "",
    owner: "",
    date: "",
    time: "",
    technician: "",
    technicians: [],
    reason: "",
  });

  const handleChange = (event) => {
    const value = event.target.value;
    setFormState({ ...formState, [event.target.id]: value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = { ...formState };
    delete data.technicians;
    delete data.id;

    const appointmentUrl = "http://localhost:8080/api/appointment/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
      const cleared = {
        vin: "",
        owner: "",
        date: "",
        time: "",
        technician: "",
        reason: "",
      };
      setFormState(cleared);
    } else {
      console.error(response);
    }
  };

  useEffect(() => {
    if (formState.technicians && formState.technicians.length === 0) {
      const fetchData = async () => {
        const url = "http://localhost:8080/api/technician/";
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setFormState({ ...formState, technicians: data.technicians });
        }
      };

      fetchData();
    }
  }, [formState]);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>New Appointment</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input
                value={formState.vin}
                onChange={handleChange}
                placeholder="VIN"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formState.owner}
                onChange={handleChange}
                placeholder="Name"
                required
                type="text"
                name="owner"
                id="owner"
                className="form-control"
              />
              <label htmlFor="owner">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formState.date}
                onChange={handleChange}
                placeholder="Date"
                required
                type="text"
                name="date"
                id="date"
                className="form-control"
              />
              <label htmlFor="date">Date</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formState.time}
                onChange={handleChange}
                placeholder="Time"
                required
                type="text"
                name="time"
                id="time"
                className="form-control"
              />
              <label htmlFor="time">Time</label>
            </div>
            <div className="mb-3">
              <select
                value={formState.technician}
                onChange={handleChange}
                placeholder="Technician"
                required
                type="text"
                name="technician"
                id="technician"
                className="form-control"
              >
                <option value="">Technician</option>
                {formState.technicians?.map((technician) => {
                  return (
                    <option key={technician.id} value={technician.id}>
                      {technician.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formState.reason}
                onChange={handleChange}
                placeholder="Reason"
                required
                type="text"
                name="reason"
                id="reason"
                className="form-control"
              />
              <label htmlFor="reason">Reason</label>
            </div>
            <div className="form-group">
              <button type="submit" className="btn btn-primary btn-block">
                Create Appointment
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default AppointmentForm;
