import React, { useState, useEffect } from "react";

const SalesPeopleList = () => {
  const [salesPeople, setSalesPeople] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const response = await fetch("http://localhost:8090/sales/sales_people/");
      if (response.ok) {
        const data = await response.json();
        setSalesPeople(data.sales_people);
      } else {
        console.error(response);
      }
    }

    fetchData();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Employee Number</th>
        </tr>
      </thead>
      <tbody>
        {salesPeople.map((salesPerson) => {
          return (
            <tr key={salesPerson.employee_number}>
              <td>{salesPerson.name}</td>
              <td>{salesPerson.employee_number}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default SalesPeopleList;
