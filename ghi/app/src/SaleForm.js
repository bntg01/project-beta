import React, { useState, useEffect } from "react";

const SaleForm = () => {
  const [state, setState] = useState({
    automobile: "",
    automobiles: [],
    sales_person: "",
    sales_people: [],
    customer: "",
    customers: [],
    sale_price: "",
  });

  const handleChange = (event) => {
    const newState = {
      ...state,
      [event.target.id]: event.target.value,
    };
    setState(newState);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = { ...state };
    delete data.automobiles;
    delete data.sales_people;
    delete data.customers;
    const SALES_URL = "http://localhost:8090/sales/sales/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(SALES_URL, fetchConfig);

    const AUTOMOBILES_URL = `http://localhost:8100${data.automobile}`;
    const soldConfig = {
      method: "put",
      body: JSON.stringify({ sold: true }),
      headers: {
        "Content-Type": "application/json",
      },
    };

    await fetch(AUTOMOBILES_URL, soldConfig);

    if (response.ok) {
      setState((prevState) => ({
        ...prevState,
        automobile: "",
        sales_person: "",
        customer: "",
        sale_price: "",
      }));
    } else {
      console.error(response);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      const AUTOMOBILES_URL = "http://localhost:8100/api/automobiles/";

      const automobilesResponse = await fetch(AUTOMOBILES_URL);

      if (automobilesResponse.ok) {
        const data = await automobilesResponse.json();
        setState((prevState) => ({
          ...prevState,
          automobiles: data.autos.filter((auto) => auto.sold === false),
        }));
      } else {
        console.error(automobilesResponse);
      }

      const SALES_PEOPLE_URL = "http://localhost:8090/sales/sales_people/";

      const salesPeopleResponse = await fetch(SALES_PEOPLE_URL);

      if (salesPeopleResponse.ok) {
        const data = await salesPeopleResponse.json();
        setState((prevState) => ({
          ...prevState,
          sales_people: data.sales_people,
        }));
      } else {
        console.error(salesPeopleResponse);
      }

      const POTENTIAL_CUSTOMERS_URL =
        "http://localhost:8090/sales/potential_cutomers/";

      const potentialCustomersResponse = await fetch(POTENTIAL_CUSTOMERS_URL);

      if (potentialCustomersResponse.ok) {
        const data = await potentialCustomersResponse.json();
        setState((prevState) => ({
          ...prevState,
          customers: data.potential_customers,
        }));
      } else {
        console.error(potentialCustomersResponse);
      }
    };

    fetchData();
  }, []);

  const automobiles = state.automobiles.map((automobile) => {
    return (
      <option key={automobile.vin} value={automobile.href}>
        {automobile.vin}
      </option>
    );
  });

  const sales_people = state.sales_people.map((sales_person) => {
    return (
      <option
        key={sales_person.employee_number}
        value={sales_person.employee_number}
      >
        {sales_person.name}
      </option>
    );
  });

  const customers = state.customers.map((customer) => {
    return (
      <option key={customer.name} value={customer.id}>
        {customer.name}
      </option>
    );
  });

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new sale</h1>
          <form onSubmit={handleSubmit} id="create-sale-form">
            <div className="mb-3">
              <select
                onChange={handleChange}
                value={state.automobile}
                required
                name="automobile"
                id="automobile"
                className="form-select"
              >
                <option value="">Choose an automobile</option>
                {automobiles}
              </select>
            </div>
            <div className="mb-3">
              <select
                onChange={handleChange}
                value={state.sales_person}
                required
                name="sales_person"
                id="sales_person"
                className="form-select"
              >
                <option value="">Choose a sales person</option>
                {sales_people}
              </select>
            </div>
            <div className="mb-3">
              <select
                onChange={handleChange}
                value={state.customer}
                required
                name="customer"
                id="customer"
                className="form-select"
              >
                <option value="">Choose a customer</option>
                {customers}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleChange}
                value={state.sale_price}
                placeholder="Sale price"
                required
                type="number"
                name="sale_price"
                id="sale_price"
                className="form-control"
              />
              <label htmlFor="sale_price">Sale price</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default SaleForm;
