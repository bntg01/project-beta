# CarCar

Team:

- Cody - Service
- Ben - Sales

## Design

Creating a program that a car dealership will be using to service cars that come in, and the cars that they have for sale. Inventory, Sales, and Services are bounded contexts that will be microservices. Everything will be shared with a AutomobileVO that links the automobile with the inventory.

## Service microservice

Explain your models and integration with the inventory
microservice, here.

Made a model for technician and appointments.
We use automobile Vo to get the info on the cars.
Poller, we need the automobile vo to aquire data from inventory views

For the front end we need a service list what lists appointments in the system
Service history on a specific vin
Technician form to make a new tech
form for making service appointments

## Sales microservice

There are models for a sales person (SalesPerson), a potential customer (PotentialCustomer), and a Sale (Sale)
There is also a value object model for the automobile data recieved from the poller

The poller makes a get request to the inventory api to automobiles
for each automobile in the response, it creates an AutomobileVO value object

The front end includes a list of sales people, potential customers, sales, and a list of sales filterable by sales person
It also includes forms to create new sales people, potential customers, and sales
